package Proxy;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.NotBoundException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Enumeration;

import lib.ProxyInterface;

public class Proxy {
	public static void main(String[] args) throws NotBoundException, IOException{
		Enumeration<InetAddress> addresses=NetworkInterface.getByName("eth1").getInetAddresses();
		InetAddress nextAddress=null;
		while(addresses.hasMoreElements()){
			nextAddress=addresses.nextElement();
			if(!(nextAddress instanceof Inet4Address)) nextAddress=null;
		}
//		se non ho trovato la scheda di rete mi accontento del metodo statico di InetAddress getLocalHost
		if(nextAddress==null) {
			nextAddress=InetAddress.getLocalHost();
		}
		
		String localIP = nextAddress.getHostAddress();
		System.setProperty("java.rmi.server.hostname", localIP);
		
		BufferedReader ausi = new BufferedReader (new FileReader("IpRegistry"));
		String lettura = ausi.readLine();
		ausi.close();
		
		Registry rmiUtenti = LocateRegistry.getRegistry(lettura, 11112);
		if(rmiUtenti == null ) return;
		
		ProxyInterface Proxy =  (ProxyInterface) rmiUtenti.lookup("proxy");
		Proxy.RegistraProxy(localIP);
		//socket per le accettazioni
		
		ServerSocket srvr=new ServerSocket(1115);
		//ciclio accettazione client
		while(true){
			try{
				Socket sock=srvr.accept();
				new Thread(new ProxyClient(sock)).start();
			}catch(IOException e){
				srvr.close();
			}
		}
	}
}
