package Proxy;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

public class ProxyClient implements Runnable {
 Socket socket;
//stream di lettura scrittura
 DataInputStream inputSData;
 OutputStream oputS;
 DataOutputStream outputSData;

	public ProxyClient(Socket sock) throws IOException {
		socket=sock;
		inputSData=new DataInputStream(socket.getInputStream());
		outputSData=new DataOutputStream(socket.getOutputStream());
		}

//	sequenze di messaggi
//	1)nome del richiedente il serivizio
//	2)tipo di servizio SND o RCV
//	 3) se � SND
//		3.1) nome del destinatario
//		3.1.1) mex
//		se � RCV
//		3.2) niente
	public void run() {
		try {
//		nome dell'user che richiede un servizio
		String nome = inputSData.readUTF();
//		ho due servizi offerti
//		1)SND inviare un messaggio a un utente offline
//		2)RCV ricevere i messaggi ricevuti da offline
		
		if(inputSData.readUTF().equals("SND"))
			sndOff(nome);
		else
			rcvOff(nome);
		
		}
		catch(IOException e){
			e.printStackTrace();
		}
		
	}

	private void rcvOff(String nome) throws IOException {
//		lista dei file ottenuti dalla stringa nome
		String path = nome+"/"; 
		String files;
		File folder = new File(path);
		File[] listaFile = folder.listFiles();
//		se ho la lsita dei file ho anche dei messaggi altrimenti invia un messaggio di avviso
		if(listaFile!=null){
			for (int i = 0; i < listaFile.length; i++){ 
				if (listaFile[i].isFile()) {
					files = listaFile[i].getName();
					outputSData.writeUTF(files);
//					bufferread per leggere il file
					BufferedReader input = new BufferedReader (new FileReader(path+files));
					String letta;
					
					while((letta=input.readLine())!=null){
						outputSData.writeUTF(letta);
					}
					outputSData.writeUTF("fine di questa chat");
					input.close();
//					elimino questo file
					listaFile[i].delete();
				}
			}
		}
		else{
			outputSData.writeUTF("EOF");
		}
		outputSData.writeUTF("EOF");
		outputSData.close();
		inputSData.close();
		socket.close();
	}

	private void sndOff(String mittente) throws IOException {
//		destinatario del messaggio
		String dest = inputSData.readUTF();
//		dest directory dei file nominati mit di messaggi ricevuti da ogni mit
		File fileoff=new File(dest+"/"+mittente);
		String mex;
//		se c'� veraemten un mex lo scrivo altrimenti non � stato inviato niente
		try{
			mex=inputSData.readUTF();
		}
		catch(IOException e){
			outputSData.close();
			inputSData.close();
			socket.close();
			return;
		}
		FileWriter fileW;
		try{
			fileW= new FileWriter(fileoff,true);
		}
//		non ho trovato la directory sicche la creo
		catch(FileNotFoundException e){
			fileoff.getParentFile().mkdirs();
			fileW= new FileWriter(fileoff,true);

		}	
//		scrivo il mex
		fileW.write(mex);
		fileW.close();
		outputSData.close();
		inputSData.close();
		socket.close();
		return;
		
	}

}
